
<xsl:template match="div[@class='nwXsltMacro_Diagram']">
    <xsl:variable name="path" select="p[@class='nwXsltMacro_Diagram_image_path']"/>
    <xsl:variable name="caption" select="p[@class='nwXsltMacro_Diagram_caption']"/>

    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">diagram</xsl:attribute>
        <xsl:element name="img">
            <xsl:attribute name="class">diagram</xsl:attribute>
            <xsl:attribute name="src">$mediaLink(relativePath='/diagrams/<xsl:value-of select="$path"/>')$</xsl:attribute>
        </xsl:element>
        <xsl:if test="$caption">
            <xsl:element name="p">
                <xsl:attribute name="class">caption</xsl:attribute>
                <xsl:value-of select="$caption"/>
            </xsl:element>
        </xsl:if>
    </xsl:element>
</xsl:template>

