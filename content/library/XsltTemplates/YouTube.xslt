
<xsl:template match="div[@class='nwXsltMacro_YouTube']">
    <xsl:variable name="id" select="p[@class='nwXsltMacro_YouTube_id']"/>
    <xsl:variable name="caption" select="p[@class='nwXsltMacro_YouTube_caption']"/>

    <xsl:variable name="url">http://www.youtube-nocookie.com/v/<xsl:value-of select="$id"/>?color2=FBE9EC&amp;version=3</xsl:variable>
    <xsl:element name="div" namespace="">
        <xsl:attribute name="class">diagram</xsl:attribute>
        <xsl:element name="div" namespace="">
            <xsl:attribute name="style">width:640px; padding: 10px; background-color: #2a3a5c; color: #c1cfea; margin-left: auto; margin-right: auto</xsl:attribute>
            <xsl:element name="p">
                By clicking on the "Play" button below, you agree to the
                <xsl:element name="a">
                    <xsl:attribute name="href">http://www.google.com/policies/technologies/cookies/</xsl:attribute>
                    Google cookie policy
                </xsl:element>
                .
            </xsl:element>
        </xsl:element>
        <xsl:element name="object">
            <xsl:attribute name="type">application/x-shockwave-flash</xsl:attribute>
            <xsl:attribute name="style">width:660px; height:400px</xsl:attribute>
            <xsl:attribute name="data"><xsl:value-of select="$url"/></xsl:attribute>
            <xsl:element name="param">
                <xsl:attribute name="name">movie</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="$url"/></xsl:attribute>
            </xsl:element>
            <xsl:element name="param">
                <xsl:attribute name="name">allowFullScreen</xsl:attribute>
                <xsl:attribute name="value">true</xsl:attribute>
            </xsl:element>
            <xsl:element name="param">
                <xsl:attribute name="name">allowscriptaccess</xsl:attribute>
                <xsl:attribute name="value">always</xsl:attribute>
            </xsl:element>
        </xsl:element>
        <xsl:if test="$caption">
            <xsl:element name="p">
                <xsl:attribute name="class">caption</xsl:attribute>
                <xsl:value-of select="$caption"/>
            </xsl:element>
        </xsl:if>
    </xsl:element>
</xsl:template>
