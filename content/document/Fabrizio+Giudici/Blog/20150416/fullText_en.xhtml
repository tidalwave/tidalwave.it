<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" media="screen" href="../../../../library/css/editing.css" type="text/css" />
  </head>
  <body>
    <p>This is a follow up to <a href="$nodeLink(relativePath='/Fabrizio Giudici/Blog', contentRelativePath='/Fabrizio Giudici/Blog/Design/20150409')$">a
        previous post</a> in which I described a refactoring of a piece of real-world code that took advantage of some
      Java 8 constructs. You should first refer to that post to understand the context of the discussion and a few hints
      to understand what's the purpose of the code I'm talking about.</p>
    <p>Below there is the original chunk of code that has been refactored. Since it came after a previous refactoring
      where <code>Metadata</code> was fitted with <code>Optional</code>, we see things such as <code>if
        (artist.isPresent())</code> in place of <code>if (artist != null)</code>, but, as I stated in my previous post,
      this kind of refactoring is unsatisfactory. There's a line in which a simple conditional has been replaced by <code>Optional.orElse()</code>,
      but - again - it's just a little step forward. Let's see how it can be replaced by something better, possibly
      totally erasing the five major conditional branches in the listing.</p>
    <pre class="prettyprint lang-java">
public void importFallbackTrackMetadata (final @Nonnull MediaItem mediaItem, final @Nonnull URI trackUri)
  {
    log.debug("importFallbackTrackMetadata({}, {})", mediaItem, trackUri);
 
    final Metadata metadata = mediaItem.getMetadata();
    StatementManager.Builder builder = statementManager.requestAddStatements();
 
    final Optional&lt;String&gt; title = metadata.get(Metadata.TITLE);<br /><br />    if (artist.isPresent()) <br />      {<br />        builder = builder.with(trackUri, DC.TITLE,   literalFor(title))
                         .with(trackUri, RDFS.LABEL, literalFor(title));
      }<br />
    final Optional&lt;String&gt; artist = metadata.get(Metadata.ARTIST);
    URI artistUri = null;
 
    if (artist.isPresent()) 
      {
        final String artistName = artist.get();
        artistUri = localArtistUriFor(idCreator.createSha1("ARTIST:" + artistName));
        builder = builder.with(trackUri, FOAF.MAKER, artistUri);
 
        if (seenArtistUris.putIfAbsent(artistUri, true) == null)
          {
            final Value nameLiteral = literalFor(artistName);
            builder = builder.with(artistUri, RDF.TYPE,   MO.C_MUSIC_ARTIST)
                             .with(artistUri, FOAF.NAME,  nameLiteral)
                             .with(artistUri, RDFS.LABEL, nameLiteral);
          }
      }
 
    final MediaFolder parent = (MediaFolder)mediaItem.getParent();
    final String recordTitle = metadata.get(Metadata.ALBUM).orElse(parent.getPath().toFile().getName());
    final URI recordUri = localRecordUriFor(idCreator.createSha1("RECORD:" + recordTitle));
 
    if (seenRecordUris.putIfAbsent(recordUri, true) == null)
      {
        final Value titleLiteral = literalFor(recordTitle);
        builder = builder.with(recordUri, RDF.TYPE,         MO.C_RECORD)
                         .with(recordUri, DC.TITLE,         titleLiteral)
                         .with(recordUri, RDFS.LABEL,       titleLiteral)
                         .with(recordUri, MO.P_MEDIA_TYPE,  MO.C_CD);
 
        if (artist.isPresent())
          {
            builder = builder.with(recordUri, FOAF.MAKER, artistUri);
          }
      }
 
    builder.with(recordUri, MO.P_TRACK, trackUri)<br />           .publish();
  }
</pre>
    <p>The logics of this piece of code is:</p>
    <ul>
      <li>given a musical track, retrieve its title and the name of the artist (both of them can be present or not);</li>
      <li>if the title is present, add two statements for it;</li>
      <li>if the artist is present, create an unique, local id for him, and associate him to the track;</li>
      <li>if it's the first time we see the artist, add some further statement to describe the artist;</li>
      <li>try to extract the title of the record that contains the track; if it's not present, the name of the folder
        containing the file will be used;</li>
      <li>if it's the first time we see the record, add some further statements to describe it;</li>
      <li>if both the artist and the record exist, add another statement to associate them;</li>
      <li>at last, add a statement to relate the track with the record.</li>
    </ul>
    <p>It seems that the refactoring is tricky: not only because of the conditionals, but also because of the
      side-effect of <code>putIfAbsent()</code>. I recall that this method of a <code>ConcurrentMap</code> is capable
      of an atomic operation of putting a new key into a map only if it's not present (the atomicity makes <code>synchronized</code>
      not needed and improves the performance). The method returns <code>null</code> if the key was actually inserted.</p>
    <p>In the space of a short lunch break I was able to refactor the whole method into:</p>
    <pre class="prettyprint lang-java">public void importFallbackTrackMetadata (final @Nonnull MediaItem mediaItem, final @Nonnull URI trackUri)
  {
    log.debug("importFallbackTrackMetadata({}, {})", mediaItem, trackUri);
 
    final Metadata metadata = mediaItem.getMetadata();
 
    final Optional&lt;String&gt; title      = metadata.get(Metadata.TITLE);
    final Optional&lt;String&gt; artistName = metadata.get(Metadata.ARTIST);
    final MediaFolder parent          = (MediaFolder)mediaItem.getParent();
    final String recordTitle          = metadata.get(Metadata.ALBUM).orElse(parent.getPath().toFile().getName());
 
    final Optional&lt;URI&gt; artistUri = artistName.map(name -&gt; localArtistUriFor(idCreator.createSha1("ARTIST:" + name)));
    final URI recordUri           = localRecordUriFor(idCreator.createSha1("RECORD:" + recordTitle));
 
    final Optional&lt;URI&gt; newArtistUri  = seenArtistUris.putIfAbsentAndGetNewKey(artistUri, true);
    final Optional&lt;URI&gt; newRecordUri  = seenRecordUris.putIfAbsentAndGetNewKey(recordUri, true);
 
    statementManager.requestAddStatements()
                    .with(trackUri,     DC.TITLE,         literalFor(title))
                    .with(trackUri,     RDFS.LABEL,       literalFor(title))
                    .with(trackUri,     FOAF.MAKER,       artistUri)
 
                    .with(recordUri,    MO.P_TRACK,       trackUri)
 
                    .with(newArtistUri, RDF.TYPE,         MO.C_MUSIC_ARTIST)
                    .with(newArtistUri, RDFS.LABEL,       literalFor(artistName))
                    .with(newArtistUri, FOAF.NAME,        literalFor(artistName))
 
                    .with(newRecordUri, RDF.TYPE,         MO.C_RECORD)
                    .with(newRecordUri, RDFS.LABEL,       literalFor(recordTitle))
                    .with(newRecordUri, DC.TITLE,         literalFor(recordTitle))
                    .with(newRecordUri, MO.P_MEDIA_TYPE,  MO.C_CD)
                    .with(newRecordUri, FOAF.MAKER,       artistUri)
                    .publish();
  }
</pre>
    <p>Let's first discuss the qualities of the refactored method:</p>
    <ol>
      <li>it's shorter;</li>
      <li>five conditional branches have been removed and, apparently, there is less complexity to understand;</li>
      <li>it was possible to completely partition the method in three parts: first the retrieval of metadata, then the
        test about whether the artist and record were already registered and at last the creation of all the statements.
        In particular, the latter part made it possible to write a regular fluent builder section, without repeating
        chunks of <code>builder = builder.with(...)</code> all around.</li>
    </ol>
    <p>The refactoring was essentially based on a single trick: replacing the conditionals about data items with <code>Optional</code>
      wrapping those data items. Hence we have a <code>Optional&lt;URI&gt;</code> <code>artistUri</code> and <code>recordUri</code>.
      But even the conditions “was the artist/record already inserted” were mapped into an <code>Optional</code>: in
      fact, <code>newArtistUri</code> and <code>newRecordUri</code> might be not present (even though <code>artistUri</code>
      is). In other words, I split the concepts “we have an artist id” / “we have a new artist id” into two seperate <code>Optional</code>s.</p>
    <p>This came with a cost, that is adapting the existing facility methods and the builder for accepting <code>Optional</code>.
      The facility method <code>literalFor()</code> has been overloaded to accept an <code>Optional&lt;String&gt;</code>
      and return an <code>Optional&lt;Value&gt;</code>; if the string is not present, the value won't be present too:</p>
    <pre class="prettyprint lang-java">@Nonnull
public static Optional&lt;Value&gt; literalFor (final Optional&lt;String&gt; optionalString) 
  {
    return optionalString.map(string -&gt; literalFor(string));
  }
</pre>
    <p> </p>
    <p>The new builder has got two overloaded methods, which accept <code>Optional</code> elements; if any element of a
      subject-predicate-object triple is missing, no statement will be added:</p>
    <pre class="prettyprint lang-java">@Nonnull
public Builder with (final @Nonnull Optional&lt;? extends Resource&gt; optionalSubject,
                     final @Nonnull URI predicate,
                     final @Nonnull Value object)
  {
    return optionalSubject.map(subject -&gt; with(subject, predicate, object)).orElse(this);
  }

@Nonnull
public Builder with (final @Nonnull Optional&lt;? extends Resource&gt; optionalSubject,
                     final @Nonnull URI predicate,
                     final @Nonnull Optional&lt;? extends Value&gt; optionalObject)
  {
    return optionalObject.map(object -&gt; with(optionalSubject, predicate, object)).orElse(this);
  }

</pre>
    <div class="tip">
      <p>At first I was a bit puzzled about how to deal with the <em>two</em> <code>Optional</code>s in the latter
        method: <code>map()</code> resolves a single instance, and for a short time I was not able to think beyond
        accepting a <code>if (optionalSubject.isPresent() &amp;&amp; optionalObject.isPresent())</code>. The solution
        is simple: <em>reduction</em>. Resolve one problem and delegate the resolution of the other to another method.
        In other words, I would have introduced the former method with a single <code> Optional</code> even though
        there was not a direct need in the original code.</p>
    </div>
    <p>Some further work needed to be done with <code>ConcurrentMap</code>, because this class hasn't been updated to
      deal with <code>Optional</code>. So I created a specialisation:</p>
    <pre class="prettyprint lang-java">public class ConcurrentHashMapWithOptionals&lt;K, V&gt; extends ConcurrentHashMap&lt;K, V&gt;
  {
    @Nonnull
    public Optional&lt;K&gt; putIfAbsentAndGetNewKey (final @Nonnull Optional&lt;K&gt; optionalKey, final @Nonnull V value)
      {
        return optionalKey.flatMap(key -&gt; putIfAbsentAndGetNewKey(key, value));
      }

    @Nonnull
    public Optional&lt;K&gt; putIfAbsentAndGetNewKey (final @Nonnull K key, final @Nonnull V value)
      {
        return (putIfAbsent(key, value) == null) ? Optional.of(key) : Optional.empty();
      }
  }
</pre>
    <p>Not only it accepts an <code>Optional</code> key, but it also returns an <code>Optional</code> value in place
      of the nullable returned by <code>putIfAbsent()</code>. Also the semantics has been slightly changed (hence the
      new name <code>putIfAbsentAndGetNewKey()</code>): it returns the current key (not the previous one) if it has
      been added, otherwise an empty <code>Optional</code>. </p>
    <p>In the end, I think that the refactoring was good.</p>
    <p>As in the previous case, you save some lines and gain some readability in the main code, but at the expense of
      some more lines of code behind the scenes. In this case, the main reason for this is the fact that not all the
      utility classes in the Java runtime have been retrofitted for working with <code>Optional</code>; in other words,
      we're filling a gap left by Oracle. On the other hand, the new class is totally reusable in similar cases.</p>
    <p>Playing the devil's advocate, one might question whether the refactored code is <em>really</em> more readable
      for everybody, including those not aware of the new features in Java 8. While a conditional is something that is
      well understood by any programmer, and it immediately stands out while reading, in this case one might be
      surprised by the fact that, while all the statement creations looks similar because they are all passed in methods
      called <code>with(...)</code>, some of them might be not created at all. It's a sort of violation of the <a href="http://en.wikipedia.org/wiki/Principle_of_least_astonishment">Principle
        of Least Astonishment</a> applied to human understanding of code. Sure, reading <code>Optional</code> should be
      a surprise for people who don't know Java 8 and push them to search for the documentation, but its presence might
      be unnoticed during a skim through the code that focuses on the section where statements are created.</p>
    <p>While I'd like to avoid a comment, because readable code shoundn't need comments, perhaps it suffices to make the
      thing more evident by renaming the <code>with()</code> methods that accept an <code>Optional</code> into a <code>withOptional()</code>:</p>
    <pre class="prettyprint lang-java">public void importFallbackTrackMetadata (final @Nonnull MediaItem mediaItem, final @Nonnull URI trackUri)
  {
    log.debug("importFallbackTrackMetadata({}, {})", mediaItem, trackUri);
 
    final Metadata metadata = mediaItem.getMetadata();
 
    final Optional&lt;String&gt; title      = metadata.get(Metadata.TITLE);
    final Optional&lt;String&gt; artistName = metadata.get(Metadata.ARTIST);
    final MediaFolder parent          = (MediaFolder)mediaItem.getParent();
    final String recordTitle          = metadata.get(Metadata.ALBUM).orElse(parent.getPath().toFile().getName());
 
    final Optional&lt;URI&gt; artistUri = artistName.map(name -&gt; localArtistUriFor(idCreator.createSha1("ARTIST:" + name)));
    final URI recordUri           = localRecordUriFor(idCreator.createSha1("RECORD:" + recordTitle));
 
    final Optional&lt;URI&gt; newArtistUri  = seenArtistUris.putIfAbsentAndGetNewKey(artistUri, true);
    final Optional&lt;URI&gt; newRecordUri  = seenRecordUris.putIfAbsentAndGetNewKey(recordUri, true);
 
    statementManager.requestAddStatements()
                    .withOptional(trackUri,     DC.TITLE,         literalFor(title))
                    .withOptional(trackUri,     RDFS.LABEL,       literalFor(title))
                    .withOptional(trackUri,     FOAF.MAKER,       artistUri)
 
                    .with(        recordUri,    MO.P_TRACK,       trackUri)
 
                    .withOptional(newArtistUri, RDF.TYPE,         MO.C_MUSIC_ARTIST)
                    .withOptional(newArtistUri, RDFS.LABEL,       literalFor(artistName))
                    .withOptional(newArtistUri, FOAF.NAME,        literalFor(artistName))
 
                    .withOptional(newRecordUri, RDF.TYPE,         MO.C_RECORD)
                    .withOptional(newRecordUri, RDFS.LABEL,       literalFor(recordTitle))
                    .withOptional(newRecordUri, DC.TITLE,         literalFor(recordTitle))
                    .withOptional(newRecordUri, MO.P_MEDIA_TYPE,  MO.C_CD)
                    .withOptional(newRecordUri, FOAF.MAKER,       artistUri)
                    .publish();
  }
</pre>
    <p>The last picky remark is about measuring test coverage. Tools such as Emma or JaCoCo can detect which lines of
      code are covered by tests and which aren't, and produce a report consisting in the code listing with red and green
      markers. In the original code we have branches and we could easily detect a red conditional body in case the test
      didn't exercise it. With the refactored code we have lost this opportunity, because the branches have been masked
      inside <code>Optional</code> and its treatment inside <code>literalFor()</code> and <code>withOptional()</code>.
      I don't think any new coverage tool can be smart enough to produce a coverage report whose readability is
      comparable to the original one. In the end, I suppose it's the usual point that there's not such a thing as a free
      lunch.</p>
  </body>
</html>
