<p>One of the major benefits of Mercurial over other SCMs is the
much greater easiness of managing branches. Since you have the
whole repository cloned on your computer, you can create, modify,
close branches and switching the working area from a branch to
another without requiring a network connection; this also means
that all operations are fast.</p>
<p>To see which are the branches currently open in your project,
use the <strong>branches</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg branches</strong>
default                      694:f937409f32e9
fix-jrw-246-1                637:cbf229d7afce
1.6                          621:f9584b14a8e4
2.0                          587:d4139f45eafa
fix-JRW-120                  562:a82b7ed21b94
fix-JRW-6                    560:eb9031bec74c
fix-JRW-162-and-JRW-194      367:e75735d2055c
</pre>

<p>The command also outputs the identifier of the latest changes
performed on each branch. To see which branch you're working with,
use the <strong>branch</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg branch</strong>
default
</pre>

<p>We have already told you that to switch the workspace from a
branch to another you use the <strong>update</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg update -c default</strong> 
77 files updated, 0 files merged, 0 files removed, 0 files unresolved
</pre>

<p>or</p>
<pre class="screen">
fritz% <strong>
hg update -C default</strong>
77 files updated, 0 files merged, 0 files removed, 0 files unresolved
</pre>

<p>You can create a new branch with the <strong>branch</strong>
command and one argument:</p>
<pre class="screen">
fritz% <strong>
hg branch my-new-branch</strong>
marked working directory as branch my-new-branch
fritz% <strong>
hg commit -m "Created my-new-branch."</strong>
</pre>

<p>Please note that <em>if you don't perform the commit, the branch
is not really created</em> (the concept is that the branch is
created by the first commit which targets it).</p>
<p>The best practice adopted by all projects by <a
href="http://www.tidalwave.it">Tidalwave</a> is
&ldquo;branch-per-feature&rdquo; - while it needs some more
reasoning by the committers, it is extremely powerful and
efficient. The concept is that every work that must be done for a
bug fix or a new feature is performhed within a specific branch.
For instance, if you are going to work on issue JRW-234, you'd
create branch "fix-JRW-234". Occasionally it might happen that two
issues appear to be tightly related since from the beginning, so
you could create a single branch for both (e.g.
"fix-JRW-234-and-JRW-235"). Very trivial changes (such as fixing a
typo in a log, or adding a comment) can be done directly on the
default branch - but we insist on that &ldquo;very trivial&rdquo;.
In doubt, it's always better to branch.</p>
<p>Branches will be merged back to the default branch only when the
work is completed - which means that all the related tests
pass.</p>
<p>The advantages of this policy are:</p>
<ol>
<li>Different people can work at different issues without
interfering with each other work.</li>
<li>Issues requiring long time to be fixed won't jeopardize the
road map. For instance, if a release is going to happen in a week,
we don't need that all the work in progress is completed. Simply,
issues that are not completed yet won't be merged. This also allows
to have a fixed release plan (e.g. one release every two weeks, or
every month) independently of the current status of work. This is
especially handy for issues that require deep investigation and are
hard to fix.</li>
<li>It is possible to anticipate work on milestones requiring
extensive refactoring or API changes. For instance, it is possible
to start working on a v2.0 branch, while at the same time regular
bug fixing goes on with v1.5, 1.6 and so on. Also, it is possible
to work with bug fixes that might require extensive probing code to
find out the problem - the probing code can be even committed, and
later removed just before the work is finished.</li>
</ol>
<p>Merging to the default branch is performed with the following
sequence of commands:</p>
<pre class="screen">
fritz% <strong>
hg update -c default</strong>
77 files updated, 0 files merged, 0 files removed, 0 files unresolved
fritz% <strong>
hg merge -f -r fix-JRW-234</strong>
3 files updated, 0 files merged, 0 files removed, 0 files unresolved
(branch merge, don't forget to commit)
fritz% <strong>
hg commit -m "Merged changes from branch fix-JRW-234"</strong>
fritz% <strong>
hg update -c fix-JRW-234</strong>
127 files updated, 0 files merged, 6 files removed, 0 files unresolved
fritz% <strong>
hg commit --close-branch -m "Closed branch fix-JRW-234"</strong>
created new head
fritz% <strong>
hg update -c default</strong>
120 files updated, 0 files merged, 0 files removed, 0 files unresolved
</pre>

<p>In general, branching of course could not be straightforward in
case of conflicts. We advice about using an IDE with Mercurial
support (such as NetBeans 6.7.1) which provide visual aids for
reviewing and resolving conflicts.</p>
<p>Especially with long-running branches, it is advisable to
periodically merge with the default branch, for instance at every
release; in this way it is possible to prevent excessive divergence
from occurring, thus reducing the chances and the impact of
conflicts when the final merge is performed. The following sequence
of commands can be used for this task:</p>
<pre class="screen">
fritz% <strong>
hg update -c fix-JRW-234</strong>
77 files updated, 0 files merged, 0 files removed, 0 files unresolved
fritz% <strong>
hg merge -f -r default</strong>
3 files updated, 0 files merged, 0 files removed, 0 files unresolved
(branch merge, don't forget to commit)
fritz% <strong>
hg commit -m "Merged changes from the default branch."</strong>
</pre>
