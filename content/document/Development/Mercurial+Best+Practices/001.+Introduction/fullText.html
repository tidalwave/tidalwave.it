<p>The basic concept of a DSCM is that there are multiple, complete
repositories. While with Subversion there is only one repository
(typically at the forge) and you check out files from it, choosing
a specific branch or tagged version, with Mercurial you always have
a complete clone of the repository on your computer; in function of
what you need to do, you extract out of it the desired branch into
a working directory. That's why you don't &ldquo;check out&rdquo;
from a Mercurial repository, but you entirely
<strong>clone</strong> it:</p>
<pre class="screen">
fritz% <strong>
hg clone https://kenai.com/hg/jrawio~src</strong>
destination directory: jrawio~src
updating working directory
361 files updated, 0 files merged, 0 files removed, 0 files unresolved
</pre>

<p>You'll note that a hidden directory named .hg is created in your
target directory: that's where the clone has been stored. To select
the desired branch to work with, you use the
<strong>update</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg update -c default</strong> 
77 files updated, 0 files merged, 0 files removed, 0 files unresolved
</pre>

<p>or</p>
<pre class="screen">
fritz% <strong>
hg update -C default</strong>
77 files updated, 0 files merged, 0 files removed, 0 files unresolved
</pre>

<p>This will prepare the required files in your working directory.
If you specify the uppercase -C option, any local uncommitted
change will be lost; with the lowercase -c option, an error message
will be printed. Of course, we advice about using the latter
version most of the time. Note that what other SCMs calls the
<em>&ldquo;trunk&rdquo;</em> is instead named the
<em>&ldquo;default&rdquo;</em> branch in Mercurial; it's the branch
used for building the official releases. The update command works
both with branches and single revision numbers or tags:</p>
<pre class="screen">
fritz% <strong>
hg update -C 612</strong>
56 files updated, 0 files merged, 7 files removed, 0 files unresolved
fritz% <strong>
hg update -C 1.5.2</strong>
37 files updated, 0 files merged, 7 files removed, 0 files unresolved
</pre>

<p>The <strong>id</strong> command let you see to which changeset
the working directory is set to:</p>
<pre class="screen">
fritz% <strong>
hg update -C 612</strong>
70 files updated, 0 files merged, 0 files removed, 0 files unresolved
fritz% <strong>
hg id</strong>
22cdc4017acf+ (1.6)
fritz% <strong>
hg update -C 1.5.2</strong>
93 files updated, 0 files merged, 7 files removed, 0 files unresolved
fritz% <strong>
hg id</strong>
bda51f8fea52+ 1.5.2
fritz% <strong>
hg update -C default</strong>
120 files updated, 0 files merged, 0 files removed, 0 files unresolved
fritz% <strong>
hg id</strong>
13cc733c3fd6
fritz% <strong>
hg update -C tip</strong>
27 files updated, 0 files merged, 9 files removed, 0 files unresolved
fritz% <strong>
hg id</strong>
3f1e2f3aea37 (1.6) tip
</pre>

<p>As you can see, the id command shows you the changeset id, the
branch if any (within parentheses) and the tags, if any.
<br />
You can see which tags are defined in the project with the
<strong>tags</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg tags</strong>
tip                              694:f937409f32e9
1.5.4                            692:6e1d38eff017
1.5.3                            638:707fa855b850
1.5.2                            568:bda51f8fea52
1.5.1                            373:c4b375a8ef3c
</pre>

<p>When you perform local changes, you <strong>commit</strong> them
as you would do with other SCMs:</p>
<pre class="screen">
fritz% <strong>
hg commit -m "Commit message."</strong>
</pre>

<p>Changes are committed only to your local repository, so other
people won't see them. Thanks to the fact that changes are locally
committed, you can even <strong>rollback</strong> the last commit
with:</p>
<pre class="screen">
fritz% <strong>
hg rollback</strong>
rolling back last transaction
</pre>

<p>Rollback can be done only once after every commit. If you need
to erase a higher number of commits, you can use the
<strong>strip</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg strip -f 687</strong>
81 files updated, 0 files merged, 0 files removed, 0 files unresolved
saving bundle to /private/tmp/src/.hg/strip-backup/8823e28e8eb5-backup
</pre>

<p>You can check which outgoing changes are pending with the
<strong>outgoing</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg outgoing</strong>
comparing with https://kenai.com/hg/jrawio~src
searching for changes
changeset:   689:166e784caab1
parent:      687:70982f05332b
user:        fabriziogiudici &lt;fabrizio dot giudici at tidalwave dot it&gt;
date:        Tue Aug 25 13:21:58 2009 +0200
summary:     Changed version number.

changeset:   690:51f2a49661cf
tag:         tip
user:        fabriziogiudici &lt;fabrizio dot giudici at tidalwave dot it&gt;
date:        Tue Aug 25 13:22:13 2009 +0200
summary:     Updated the license.
</pre>

<p>The latest change in a branch is called the
<em>&ldquo;tip&rdquo;</em> and can be see with the
<strong>tip</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg tip</strong>
changeset:   694:f937409f32e9
tag:         tip
user:        hudson
date:        Sun Aug 23 21:12:51 2009 +0200
summary:     [maven-release-plugin] prepare for next development iteration
</pre>

<p>Note that revision numbers in Mercurial are quite different from
Subversion. The simple incremental number (e.g. 694) is only valid
in your local repository and for a limited time; it might change
every time you synchronize with the remote repository, if other
people made changes too. The good revision identifier that is
always valid is the alphanumeric string (e.g. f937409f32e9).
Mercurial uses SHA-1 hashes for identifying any revision; what
you're seeing is the leading part of a SHA-1 hash, which is handier
than the full thing, still keeping very low chances of
collisions.</p>
<p>You can synchronize your changes (so others can see them) with
the <strong>push</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg push</strong>
pushing to https://kenai.com/hg/jrawio~src
searching for changes
adding changesets
adding manifests
adding file changes
added 7 changesets with 72 changes to 78 files (+1 heads)
</pre>

<p>which will update the remote repository. This operation might
fail if somebody else have performed incompatible changes in the
meantime. In this case, you'll be forced to first sync your local
repository with them, merge them and at last performing the push.
You can see whether there are incoming changes with the
<strong>incoming</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg incoming</strong>
comparing with https://kenai.com/hg/jrawio~src
searching for changes
changeset:   692:6e1d38eff017
tag:         1.5.4
user:        hudson
date:        Sun Aug 23 21:12:15 2009 +0200
summary:     [maven-release-plugin] prepare release 1.5.4

changeset:   693:6a22b7d32402
user:        hudson
date:        Sun Aug 23 21:12:35 2009 +0200
summary:     [maven-release-plugin]  copy for tag 1.5.4

changeset:   694:f937409f32e9
tag:         tip
user:        hudson
date:        Sun Aug 23 21:12:51 2009 +0200
summary:     [maven-release-plugin] prepare for next development iteration
</pre>

<p>and you get the remote changes with the <strong>pull</strong>
command:</p>
<pre class="screen">
fritz% <strong>
hg pull</strong>
pulling from https://kenai.com/hg/jrawio~src
searching for changes
adding changesets
adding manifests
adding file changes
added 8 changesets with 18 changes to 4 files
(run 'hg update' to get a working copy)
</pre>

<p>You can see the history of the project with the
<strong>log</strong> command:</p>
<pre class="screen">
fritz% <strong>
hg log --limit 2</strong>
changeset:   694:f937409f32e9
tag:         tip
user:        hudson
date:        Sun Aug 23 21:12:51 2009 +0200
summary:     [maven-release-plugin] prepare for next development iteration

changeset:   693:6a22b7d32402
user:        hudson
date:        Sun Aug 23 21:12:35 2009 +0200
summary:     [maven-release-plugin]  copy for tag 1.5.4
</pre>

<p>We advice about always using the --limit option, otherwise
Mercurial will dump the whole history of the project since the
beginning.</p>
<p>All the described commands but push, pull, outgoing and incoming
don't need a network connection, which facilitates the work in
&ldquo;roaming&rdquo; mode. Furthermore, since commits are local,
they are very fast - this facilitates a good working practice, that
is &ldquo;commit often&rdquo; - we advice about committing every
few minutes of work and pushing batch of changes when they make
sense; at least once per day. For what concerns best commit
practices, please read further about the branch management.
<br />
 Consider that it's even possible to &ldquo;collapse&rdquo;
multiple local commits into a single one, with a single
description, before pushing them; this procedure has been described
by Martin Fowler as <a
href="http://martinfowler.com/bliki/MercurialSquashCommit.html"><em>&ldquo;squash
commit&rdquo;</em></a>. We don't recommend trying it until you have
some deeper knowledge about how Mercurial works.</p>
